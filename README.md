# rapid-switch

Change your virtual desktop with the press of a key. Helpful as a panic button.

## Getting Started

### Prerequisites

```
pip install pyautogui
pip install pyxhook
pip install pyqt5
pip install basic-config-loader
```

### Installing

Download the file and put it in your $PATH

### Configuring

The configuration is read from a file called .rapidswitcher in your home directory. It should be in the form of a JSON file. See config-example.json in the source repo for more information.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
